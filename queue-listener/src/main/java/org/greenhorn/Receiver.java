/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.greenhorn;

import org.greenhorn.queue.core.Country;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 *
 * @author greenhorn
 */
@Component
public class Receiver {

    @JmsListener(destination = "country", containerFactory = "myFactory")
    public void receiveMessage(Country country) {
        System.out.println("Received name <" + country.getName() + ">");
        System.out.println("Received state <" + country.getState() + ">");
        System.out.println("Received population <" + country.getPopulation() + ">");
    }
}

