package org.greenhorn;

import org.greenhorn.queue.core.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.core.JmsTemplate;

@SpringBootApplication
public class QueueProducerApplication implements CommandLineRunner {

    @Autowired
    private JmsTemplate jmsTemplate;

    public static void main(String[] args) {
        SpringApplication.run(QueueProducerApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        // Send a message with a POJO - the template reuse the message converter
        Country country = new Country();
        country.setName("BBBBBBBBB");
        country.setState("state FFFFFFF");
        country.setPopulation(12);
        System.out.println("Sending an country message.");
        jmsTemplate.convertAndSend("country", country);
    }
}